# store our terraform state in Google Cloud
terraform {
  backend "gcs" {
    bucket  = "indent-cloud-terraform"
    prefix  = "terraform/state/production"
  }
}

variable "cloudflare_account_email" {
  type = string
  description = "Email address of the cloudflare account the domain belongs to"
}
variable "cloudflare_account_token" {
  type = string
  description = "Access token of the cloudflare account the domain belongs to"
}
variable "cloudflare_zone_id" {
  type = string
  description = "ID of the zone we are configuring in Cloudflare"
}

variable "region" {
  default = "us-east1"
}

variable "zone" {
  default = "us-east1-b"
}


# GCP is our cloud of choice so lets set that up
provider "google-beta" {
  version     = "~> 3.19"
  project     = "indent-cloud"
  region      = var.region
}

data "google_client_config" "current" {}

########################
## Kubernetes cluster ##
########################
resource "google_container_cluster" "primary" {
  name     = "indent-cluster-primary"
  location = var.zone

  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1

  master_auth {
    username = ""
    password = ""

    client_certificate_config {
      issue_client_certificate = false
    }
  }
}

resource "google_container_node_pool" "primary_nodes_1" {
  name       = "indent-cluster-primary-nodepool-1"
  location   = var.zone
  cluster    = google_container_cluster.primary.name
  node_count = 3

  node_config {
    preemptible  = true
    machine_type = "e2-small"
    disk_size_gb = 10

    metadata = {
      disable-legacy-endpoints = "true"
    }

    oauth_scopes = [
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
  }
}

provider "kubernetes" {
  version                = "~> 1.11"
  host                   = google_container_cluster.primary.endpoint
  token    = data.google_client_config.current.access_token
  load_config_file       = false

  client_certificate     = base64decode(google_container_cluster.primary.master_auth.0.client_certificate)
  client_key             = base64decode(google_container_cluster.primary.master_auth.0.client_key)
  cluster_ca_certificate = base64decode(google_container_cluster.primary.master_auth.0.cluster_ca_certificate)
}
provider "helm" {
  version        = "~> 1.1"

  kubernetes {
    host                   = google_container_cluster.primary.endpoint
    token    = data.google_client_config.current.access_token
    load_config_file       = false

    client_certificate     = base64decode(google_container_cluster.primary.master_auth.0.client_certificate)
    client_key             = base64decode(google_container_cluster.primary.master_auth.0.client_key)
    cluster_ca_certificate = base64decode(google_container_cluster.primary.master_auth.0.cluster_ca_certificate)
  }
}

# create a static external IP
resource "google_compute_address" "ingress" {
  name = "ingress-address"
}

# Add Kubernetes Stable Helm charts repo
data "helm_repository" "stable" {
  name = "stable"
  url  = "https://kubernetes-charts.storage.googleapis.com"
}

# Install Traefik Ingress using Helm Chart
resource "helm_release" "traefik" {
  name       = "traefik"
  repository = data.helm_repository.stable.url
  chart      = "traefik"
  namespace  = "kube-system"
  version    = "~> 1.86"

  set {
    name  = "appVersion"
    value = "1.7.20"
  }

  set {
    name  = "rbac.create"
    value = "true"
  }
  set {
    name  = "rbac.enabled"
    value = "true"
  }

  set {
    name  = "externalTrafficPolicy"
    value = "Local"
  }

  set {
    name  = "loadBalancerIP"
    value = google_compute_address.ingress.address
  }

  set {
    name  = "acme.enabled"
    value = "true"
  }
  set {
    name  = "acme.email"
    value = "letsencrypt@indent.cloud"
  }
  set {
    name  = "acme.challengeType"
    value = "dns-01"
  }
  values = [<<EOF
    ssl:
      enabled: true
    acme:
      enabled: true
      staging: false
      challengeType: "dns-01"
      dnsProvider:
        name:  cloudflare
        cloudflare:
          CLOUDFLARE_EMAIL: ${var.cloudflare_account_email}
          CLOUDFLARE_API_KEY: ${var.cloudflare_account_token}
      domains:
        enabled: true
        domainsList:
          - main: "*.indent.cloud"
          - sans:
            - "indent.cloud"
    EOF
  ]
}

provider "cloudflare" {
  email   = var.cloudflare_account_email
  api_key = var.cloudflare_account_token
  version = "~> 2.2"
}

resource "cloudflare_record" "root" {
  zone_id = var.cloudflare_zone_id
  name    = "@"
  value   = google_compute_address.ingress.address
  type    = "A"
  ttl     = 600
}

resource "cloudflare_record" "sub_domain" {
  zone_id = var.cloudflare_zone_id
  name    = "*"
  value   = google_compute_address.ingress.address
  type    = "A"
  ttl     = 600
}

resource "random_password" "primary_database_password" {
  length           = 24
  special          = true
  override_special = "_%@"
}

# Create Database
resource "google_sql_database_instance" "primary" {
  name             = "primary-postgres"
  database_version = "POSTGRES_11"
  region           = var.region

  settings {
    tier = "db-f1-micro"
  }
}
resource "google_sql_user" "primary-user" {
  name     = "indent-cloud"
  instance = google_sql_database_instance.primary.name
  password = random_password.primary_database_password.result
  project  = "indent-cloud" # shouldnt need this?
}
resource "google_sql_database" "indent-cloud" {
  name     = "indent-cloud"
  instance = google_sql_database_instance.primary.name
  project  = "indent-cloud" # shouldnt need this?
}

resource "google_secret_manager_secret" "primary_database_ip" {
  provider = google-beta

  secret_id = "primary_database_ip"

  replication {
    automatic = true
  }
}
resource "google_secret_manager_secret_version" "primary_database_ip" {
  provider = google-beta

  secret = google_secret_manager_secret.primary_database_ip.id

  secret_data = google_sql_database_instance.primary.ip_address.0.ip_address
}
resource "google_secret_manager_secret" "primary_database_password" {
  provider = google-beta

  secret_id = "primary_database_password"

  replication {
    automatic = true
  }
}
resource "google_secret_manager_secret_version" "primary_database_password" {
  provider = google-beta

  secret = google_secret_manager_secret.primary_database_password.id

  secret_data = random_password.primary_database_password.result
}